FROM openjdk:7-jre

WORKDIR /app

COPY target /app
COPY run.sh .

EXPOSE 8080

ENTRYPOINT ["run.sh"]
